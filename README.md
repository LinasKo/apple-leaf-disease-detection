# apple-leaf-disease-detection

Let's visit apple leaf disease detection, see if the latest models can improve over the outcomes, compared to prior models.

The goal is to produce a realistic detection model, fit to be deployed on multiple devices, while evaluating Roboflow's infrastructure. I plan to:

- Merge labelled data from multiple dataset
- Pick a SOTA model, train on augmented data
- Release the trained model
- Give a few pointers on deployment.

## Issues

I foresee the issue of ergonomics. The datasets I found mostly contain close-up images of leaves, whereas real-life
applications may use drones, robots or a wearable / handheld camera.

Ideally, the model should be small enough to work live on handheld devices.
